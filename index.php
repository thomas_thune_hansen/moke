<?php
function serve_media() {
    // Get LiveUrl from remote.url file.
    $liveUrl = trim(file_get_contents('remote.url')) . '/';

    // Path prefix
    $pathPrefix = str_replace('media/index.php', '', $_SERVER['SCRIPT_NAME']);

    // New url, where to fetch the original
    $originalUrl = str_replace($pathPrefix, $liveUrl, $_SERVER['REQUEST_URI']);
    $path = str_replace($pathPrefix, '', $_SERVER['REQUEST_URI']);

    // Try to get the file from the remote

    if (get_http_response_code($originalUrl) != "200") {
        // No dice - Kittens!
        header('location: http://placekitten.com/500/400');
    } else {
        $file = file_get_contents($originalUrl);
        // Create correct folder structure in media folder
        $pathExploded = explode('/', $path);
        $fileName = end($pathExploded);

        array_pop($pathExploded);

        $structure = implode('/', $pathExploded);

        $createFolder = dirname(__DIR__) . '/' . $structure;

        if (!is_dir($createFolder)) {
            mkdir($createFolder, 0777, true);
        }

        // Save the file
        file_put_contents($createFolder . '/' . $fileName, $file);

        // Logging
        file_put_contents(__DIR__ . '/' . 'downloaded_files', $createFolder . '/' . $fileName . "\n", FILE_APPEND);

        // Serve the newly downloaded file
        header('location: '. $_SERVER['REQUEST_URI']);
    }
}

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

function init_moke($liveUrl) {
    $htaccess = <<<EOL
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ index.php
EOL;

    file_put_contents(__DIR__ . '/.htaccess', $htaccess);

    file_put_contents(__DIR__ . '/remote.url', trim($liveUrl, '/'));
}

function usage() {
    print <<<EOL
moke - MediaOrKittEns

Execute moke from the commandline to initalize.

Usage [cli]:
php index.php [http://www.remotesite.url]

EOL;
}

function main($argv) {
    if ((php_sapi_name() === 'cli')) {
        if (!isset($argv[1])) {
            usage();
            return;
        }
        init_moke($argv[1]);
    } else {
        serve_media();
    }
}

main($argv = null);
exit;
